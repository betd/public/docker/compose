ARG VERSION=alpine
FROM docker/compose:${VERSION}

LABEL maintenainer="Pierre Pottié <pierre.pottie@gmail.com>"

RUN  apk add  --update --no-cache py-pip make git patch

