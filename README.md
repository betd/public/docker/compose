# compose

Compose est un outil permettant de définir et d'exécuter des applications Docker multi-conteneurs. Avec Compose, vous utilisez un fichier Compose pour configurer les services de votre application. Ensuite, à l'aide d'une seule commande, vous créez et démarrez tous les services à partir de votre configuration. 

https://docs.docker.com/compose/